/*********************************************************
* Subor: main.c                                          *
* Nazov projektu: IAL - Rovinnost grafu                  *
* Varianta: cislo 5                                      *
*                                                        *
* Vypracovali:                                           *
* 1. Kello Tomas,       xkello00@stud.fit.vutbr.cz  20%  *
* 2. Luptak Matej,      xlupta02@stud.fit.vutbr.cz  20%  *
* 3. Picha Vojtech,     xpicha06@stud.fit.vutbr.cz  20%  *
* 4. Skalnikova Zuzana, xskaln04@stud.fit.vutbr.cz  20%  *
* 5. Toth Viktor,       xtothv01@stud.fit.vutbr.cz  20%  *
*                                                        *
* Datum vytvorenia:         12.10.2015                   *
* Datum poslednej zmeny:    13.12.2015                   *
*********************************************************/
#include "stdio.h"
#include "stdlib.h"
#include "main.h"

void VYPIS_STRUKTURU_MAPY(MapaStruct *mapa)
{
	uint32_t i;
	printf("\n\033[0;32mVYPIS STRUKTURY\033[0m\nBodyCount: %d\n", mapa->BodyCount);
	for (i = 0; i < mapa->BodyCount; ++i)
		printf("%d ", mapa->Body[i]);

	printf("\nBodyMin3Count: %d\n", mapa->BodyMin3Count);
	for (i = 0; i < mapa->BodyMin3Count; ++i)
		printf("%d ", mapa->BodyMin3[i]);

	printf("\nBodyMin4Count: %d\n", mapa->BodyMin4Count);
	for (i = 0; i < mapa->BodyMin4Count; ++i)
		printf("%d ", mapa->BodyMin4[i]);

	printf("\nHranaCount: %d\n", mapa->HranaCount);
	for (i = 0; i < mapa->HranaCount; ++i)
		printf("%d\t %d\n", mapa->Hrana[0][i], mapa->Hrana[1][i]);
	printf("\n\nKONIEC STRUKTURY\n\n");
}

int main(int argc, char* argv[])
{
	MapaStruct Mapa;

// Inicializacia prazdnych hodnot
// tieto hodnoty by mala nacitat funkcia "nacitaj" (Viktor)
	memset(&Mapa, 0, sizeof(Mapa));


	if (!nacitaj(argc, argv, &Mapa))
	{
		printf("Nepodarilo sa nacitat mapu\n");
		return -1;
	}

	if(DEBUG)
	{
		printf("Nacitanie prebehlo v poriadku\n");
		VYPIS_STRUKTURU_MAPY(&Mapa);
	}

	int K5 = 0, K33 = 0;
	K5 = NajdiK5(&Mapa);
	K33 = NajdiK33(&Mapa);

	printf("\033[0;32m");
	if ((K5 == 0) && (K33 == 0))
	{
		printf("Zadany graf je rovinny\n");
	}
	else
	{
		if(K5 == 0)
			printf("V mape sa nasla %dx podmapa K33\n", K33);
		else
		{
			if(K33 == 0)
				printf("V mape sa nasla %dx podmapa K5\n", K5);
			else
				printf("V mape sa nasla %dx podmapa K33 a %dx podmapa K5\n", K33, K5);
		}
		printf("Graf nie je rovinny\n");
	}
	printf("\033[0m");

	freeFunc(&Mapa);
	return 0;
}