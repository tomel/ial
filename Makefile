CC=gcc
RM=rm -rf
CFLAGS=-std=c99 -Wall -Wextra -pedantic



all: mapa

a: all ./mapa

s: clean all ./mapa

debug: CFLAGS=-std=c99 -Wall -Wextra  -pedantic -g
degug: all


mapa: main.o IO.o K5.o K33.o kombinacie.o
	$(CC) $(CFLAGS) -o mapa main.o IO.o K5.o K33.o kombinacie.o

mapa.o: main.c main.h
	$(CC) $(CFLAGS) -o main.o -c main.c

IO.o: IO.c
	$(CC) $(CFLAGS) -o IO.o -c IO.c

K5.o: K5.c K5.h
	$(CC) $(CFLAGS) -o K5.o -c K5.c

K33.o: K33.c K33.h
	$(CC) $(CFLAGS) -o K33.o -c K33.c

kombinacie.o: kombinacie.c kombinacie.h
	$(CC) $(CFLAGS) -o kombinacie.o -c kombinacie.c




run: clean all
	./mapa


pack: clean
	zip mapaExport *.c *.h Makefile

clean:
	$(RM) *.o mapa mapaExport.zip

