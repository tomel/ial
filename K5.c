/*********************************************************
* Subor: K5.c                                            *
* Nazov projektu: IAL - Rovinnost grafu                  *
* Varianta: cislo 5                                      *
*                                                        *
* Vypracovali:                                           *
* 1. Kello Tomas,       xkello00@stud.fit.vutbr.cz  20%  *
* 2. Luptak Matej,      xlupta02@stud.fit.vutbr.cz  20%  *
* 3. Picha Vojtech,     xpicha06@stud.fit.vutbr.cz  20%  *
* 4. Skalnikova Zuzana, xskaln04@stud.fit.vutbr.cz  20%  *
* 5. Toth Viktor,       xtothv01@stud.fit.vutbr.cz  20%  *
*                                                        *
* Datum vytvorenia:         12.10.2015                   *
* Datum poslednej zmeny:    13.12.2015                   *
*********************************************************/
#include "K5.h"

bool FindPath5(MapaStruct *Mapa, uint32_t *k5)
{
	uint32_t a = 0;
	uint32_t b = 0;
	uint32_t c = 0;
	uint32_t d = 0;

	for (uint32_t k = 0; k < 5; k++)
	{
		a=0;b=0;c=0;d=0;
		for(uint32_t j=0;j < (Mapa->HranaCount); j++)
		{
			if ((Mapa->Hrana[0][j] == Mapa->BodyMin4[k5[k]] && Mapa->Hrana[1][j] == Mapa->BodyMin4[k5[(k+1)%5]]) ||
				(Mapa->Hrana[1][j] == Mapa->BodyMin4[k5[k]] && Mapa->Hrana[0][j] == Mapa->BodyMin4[k5[(k+1)%5]]))
				a=1;

			if ((Mapa->Hrana[0][j] == Mapa->BodyMin4[k5[k]] && Mapa->Hrana[1][j] == Mapa->BodyMin4[k5[(k+2)%5]]) ||
				(Mapa->Hrana[1][j] == Mapa->BodyMin4[k5[k]] && Mapa->Hrana[0][j] == Mapa->BodyMin4[k5[(k+2)%5]]))
				b=1;

			if ((Mapa->Hrana[0][j] == Mapa->BodyMin4[k5[k]] && Mapa->Hrana[1][j] == Mapa->BodyMin4[k5[(k+3)%5]]) ||
				(Mapa->Hrana[1][j] == Mapa->BodyMin4[k5[k]] && Mapa->Hrana[0][j] == Mapa->BodyMin4[k5[(k+3)%5]]))
				c=1;

			if ((Mapa->Hrana[0][j] == Mapa->BodyMin4[k5[k]] && Mapa->Hrana[1][j] == Mapa->BodyMin4[k5[(k+4)%5]]) ||
				(Mapa->Hrana[1][j] == Mapa->BodyMin4[k5[k]] && Mapa->Hrana[0][j] == Mapa->BodyMin4[k5[(k+4)%5]]))
				d=1;
		}
		if( a == 1 && b == 1 && c == 1 && d == 1)
			return true;
	}
	return false;
}
int NajdiK5(MapaStruct *Mapa)
{
	if (DEBUG_K5)
		printf("----  K5 ----\nBody min 4 pocet: %u (musi byt aspon 5)\n", Mapa->BodyMin4Count);
	if (Mapa->BodyMin4Count < 5)
		return 0; // najdenych 0 kombinacii

	uint32_t k5[] = {0, 1, 2, 3, 4 }; // definovanie prvej kombinacie
	uint32_t pocetKombinacii = 0; // pocet kombinacii
	uint32_t K5Count = 0; // pocet najdenych izomorvnych podgrafou s K5

	while (1)
	{// vytvara vsetky kombinacie v k5
		pocetKombinacii++;
		if(FindPath5(Mapa, k5))
		{
			if (DEBUG_K5_NASLO)
					printf("%u kombinacia nasla podgraf K5: %u %u %u %u %u (hodnoty su INDEXY do mapa->BodyMin4)\n %i - %i\n", pocetKombinacii,
	                   k5[0],
	                   k5[1],
	                   k5[2],
	                   k5[3],
	                   k5[4],
					   Mapa->BodyMin4[k5[0]],
					   Mapa->BodyMin4[k5[4]]);
			K5Count++;
		}
		if (DEBUG_K5)
			printf("%u kombinacia: %u %u %u %u %u (hodnoty su INDEXY do mapa->BodyMin4)\n", pocetKombinacii,
                   k5[0],
                   k5[1],
                   k5[2],
                   k5[3],
                   k5[4]);
		// ---------- mapa, length, MAX value
		if (!NextComb(k5,   5,      Mapa->BodyMin4Count-1))
			break;
	}
	if(DEBUG)
		printf("pocet kombinacii: %u\n", pocetKombinacii);

	return K5Count;
}