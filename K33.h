/*********************************************************
* Subor: K33.h                                           *
* Nazov projektu: IAL - Rovinnost grafu                  *
* Varianta: cislo 5                                      *
*                                                        *
* Vypracovali:                                           *
* 1. Kello Tomas,       xkello00@stud.fit.vutbr.cz  20%  *
* 2. Luptak Matej,      xlupta02@stud.fit.vutbr.cz  20%  *
* 3. Picha Vojtech,     xpicha06@stud.fit.vutbr.cz  20%  *
* 4. Skalnikova Zuzana, xskaln04@stud.fit.vutbr.cz  20%  *
* 5. Toth Viktor,       xtothv01@stud.fit.vutbr.cz  20%  *
*                                                        *
* Datum vytvorenia:         12.10.2015                   *
* Datum poslednej zmeny:    13.12.2015                   *
*********************************************************/
#ifndef K33File
#define K33File

#include "struct.h"
#include "main.h"

// hlada spojenie medzi bodom s indexom v Z do vsetkych troch bodov
// ulozenych v poli Do
// vsetky hrany su ulozene v strukture Mapa
// vracia true ak najde spojenie medzi vsetkymi spojeniami
bool FindPath3(MapaStruct *Mapa, uint32_t k3a1, uint32_t k3a2, uint32_t k3a3, uint32_t k3b1, uint32_t k3b2, uint32_t k3b3);

// nastavi k3b na indexy v rozsahu 0-5 ktore sa nevyskytuju v k3a
void set_k3b(uint32_t *k3a, uint32_t *k3b);

// hlada izomorfny podgraf K33 v zadanej Mape
// return pocet najdenych podgrafov K33
uint32_t NajdiK33(MapaStruct *Mapa);


#endif
