﻿/*********************************************************
* Subor: IO.c                                            *
* Nazov projektu: IAL - Rovinnost grafu                  *
* Varianta: cislo 5                                      *
*                                                        *
* Vypracovali:                                           *
* 1. Kello Tomas,       xkello00@stud.fit.vutbr.cz  20%  *
* 2. Luptak Matej,      xlupta02@stud.fit.vutbr.cz  20%  *
* 3. Picha Vojtech,     xpicha06@stud.fit.vutbr.cz  20%  *
* 4. Skalnikova Zuzana, xskaln04@stud.fit.vutbr.cz  20%  *
* 5. Toth Viktor,       xtothv01@stud.fit.vutbr.cz  20%  *
*                                                        *
* Datum vytvorenia:         12.10.2015                   *
* Datum poslednej zmeny:    13.12.2015                   *
*********************************************************/
#include "IO.h"
#define MAGIC 10
#define MAX_SIZE 1000

bool nacitaj(int argc, char* argv[], MapaStruct *Mapa){
    
    if (argc != 2)
    {
        printf("Nespravny pocet argumentov ... program skonci !\n");
        return false;
    }
    
    char filename[MAX_SIZE];
    strcpy(filename,argv[1]);

    FILE *file = fopen(filename, "r");
    uint32_t pocitadloHran=0;

    if (file)
    {
        if (DEBUG)
        {
            printf("FILE: %s sa nacital spravne\n",filename);
        }
        
        char nacitanyZnak;
        bool bielyZnak = false;     //novy riadok
        char predchadzajuciZnak = 0;
        bool predchadzajucaCiarka = false;

        /*
        * kontrola suboru
        */
        
        if ((nacitanyZnak = fgetc(file)) == EOF)
        {
             printf("Subor je prazdny ... program skonci !\n");
             return false;
        }    

        while ((nacitanyZnak = fgetc(file)) != EOF)
        {
            if ((nacitanyZnak >= '0' && nacitanyZnak <= '9' ) ||
                 nacitanyZnak == ' ' ||
                 nacitanyZnak == '\n' ||
                 nacitanyZnak == '\r' ||
                 nacitanyZnak == ',')  
            {
                if(nacitanyZnak >= '0' && nacitanyZnak <= '9' ){
                    predchadzajucaCiarka = false;
                }
                else if(nacitanyZnak == ',' && predchadzajucaCiarka == true){
                    fclose(file);
                    printf("Nespravny format ... program skonci !\n");
                    return false;
                }
                else if(nacitanyZnak == ','){
                    predchadzajucaCiarka = true;
                }
                if((nacitanyZnak == '\n' || nacitanyZnak == '\r') && (bielyZnak == true || predchadzajuciZnak == 0)){
                    continue;
                }
                else{
                    bielyZnak = false;
                }
                if(nacitanyZnak == '\n' || nacitanyZnak == '\r'){
                    pocitadloHran++;
                    bielyZnak = true;
                }
                predchadzajuciZnak = nacitanyZnak;
            }
            else
            {
                printf("Nespravny format ... program skonci !\n");
                return false;
                  
            }
        }
        if(nacitanyZnak == EOF && bielyZnak == false){
            pocitadloHran++;
        }



        Mapa->HranaCount = pocitadloHran;
        Mapa->Hrana[0] = malloc(sizeof(uint32_t)*pocitadloHran);
        if (Mapa->Hrana[0] == NULL){
            perror("Nepodarilo sa alokovat miesto v pamati ... program skonci !\n");
            return false; // TODO error
        }

        Mapa->Hrana[1] = malloc(sizeof(uint32_t)*pocitadloHran);
        if (Mapa->Hrana[1] == NULL){
            perror("Nepodarilo sa alokovat miesto v pamati ... program skonci !\n");
            return false; // TODO error
        }

        //-------------------------------docasne-------------------------------
        uint32_t *Hrana0 = malloc(sizeof(uint32_t)*pocitadloHran);
        if (Hrana0 == NULL){
            perror("Nepodarilo sa alokovat miesto v pamati ... program skonci !\n");
            return false; // TODO error
        }

        uint32_t *Hrana1 = malloc(sizeof(uint32_t)*pocitadloHran);
        if (Hrana1 == NULL){
            perror("Nepodarilo sa alokovat miesto v pamati ... program skonci !\n");
            return false; // TODO error
        }
        //-------------------------------docasne-------------------------------



        rewind(file);           //spat na zaciatok file-u

        if ( file ){
            uint32_t i = 0;
            uint32_t j = 0;
            uint32_t k = 0;
            uint32_t controll = 0;
            char buffer[BUFSIZ], *ptr;
            bool ok = true;

            /*
            * Nacitanie suboru po riadkoch
            */
            for ( i = 0; fgets(buffer, sizeof buffer, file); ++i){
                ptr = buffer;
                controll = (uint32_t)strtol(ptr, &ptr, 10);
                if (controll != 0)
                {
                    Mapa->Hrana[0][j] = controll;
                }
                ++ptr;
                controll = (uint32_t)strtol(ptr, &ptr, 10);
                if (controll != 0)
                {
                    Mapa->Hrana[1][j] = controll;
                    j++;
                }
                ++ptr;
            }
            fclose(file);

            /*
            *vymazem dve UPLNE rovnake zaznamy: 1 2
            *                                   1 2
            *                   ALE             2 1  nemazem
            */

            for ( i = 0; i < Mapa->HranaCount; i++)
            {
                if (Mapa->Hrana[0][i] != Mapa->Hrana[1][i])
                {
                    for ( j = i+1, ok=true; j < Mapa->HranaCount; j++)
                    {
                        if (Mapa->Hrana[0][i] == Mapa->Hrana[0][j] && Mapa->Hrana[1][i] == Mapa->Hrana[1][j] )
                        {
                            ok = false;
                        }
                    }
                    if (ok == true)
                    {
                        Hrana0[k] = Mapa->Hrana[0][i];
                        Hrana1[k] = Mapa->Hrana[1][i];

                        k++;
                    }
                }
            }

            /*
            * vymazem aj toto   1 2
            *                   2 1
            * zostanie IBA 2 1
            */

            Mapa->HranaCount = k;
            k=0;
            for ( i = 0; i < Mapa->HranaCount; i++)
            {
                for ( j = i+1, ok=true; j < Mapa->HranaCount; j++)
                {
                    if (Hrana1[i] == Hrana0[j] && Hrana0[i] == Hrana1[j])
                    {
                        ok = false;
                    }
                }
                if (ok == true)
                {
                    Mapa->Hrana[0][k] = Hrana0[i];
                    Mapa->Hrana[1][k] = Hrana1[i];

                    k++;
                }
            }

            /*
            * dealokujem pamat Hrana0, Hrana1
            */
            if (Hrana0 != NULL)
                free(Hrana0);
            if (Hrana1 != NULL)
                free(Hrana1);

            Mapa->HranaCount = k;

            /*
            * Print the data in 'Mapa.Hrany'.
            */

            if(DEBUG)
                for (i = 0; i < k; ++i ){
                    printf("Mapa->Hrana[0][%u]  |  Mapa->Hrana[1][%u] : %4u %4u\n", i,i, Mapa->Hrana[0][i], Mapa->Hrana[1][i]);
                }

            if (DEBUG)
            {
                printf("Pocet hran (HranaCount) je: %d\n", Mapa->HranaCount);
            }

            /*
            *   Nacitam uplne vsetky zaznamy do pola
            */
            uint32_t *vsetkyBody;
            vsetkyBody = malloc(sizeof(uint32_t)*((Mapa->HranaCount)*2)-2);

            for ( i = 0, j = 0; i <= (Mapa->HranaCount - 1); i++)
            {
                vsetkyBody [j] = Mapa->Hrana[0][i];
                j++;
                vsetkyBody [j] = Mapa->Hrana[1][i];
                j++;
            }

            /*
            * Zoradim pole od najvacsie az po najmensie
            */
            shellSort(vsetkyBody, ((Mapa->HranaCount)*2));

            uint32_t NewLength = 1;

            /*
            * Vymazem duplikaty v poli
            */
            for(i=1; i< ((Mapa->HranaCount)*2); i++){

               for(j=0; j< NewLength ; j++)
               {

                  if(vsetkyBody[i] == vsetkyBody[j])
                  break;
               }

              if (j==NewLength )
                  vsetkyBody[NewLength++] = vsetkyBody[i];
            }

            NewLength--;
            Mapa->BodyCount = NewLength+1;

            /*
            * Alokujem pole Body na presnu velkost a doplnim ho
            */
            Mapa->Body= malloc(sizeof(uint32_t)*NewLength);

            for (uint32_t i = 0; i <= NewLength; ++i)
            {
                Mapa->Body[i] = vsetkyBody[i];
            }

            if (DEBUG)
            {
                printf("Pocet bodov (BodyCount) je: %d\n", Mapa->BodyCount);

                printf("Vsetky neopakujuce sa body, su zostupne (*Body): \n");
                for (uint32_t i = 0; i <= NewLength; ++i)
                {
                    printf("\t\t\t\t\t%d\n",Mapa->Body[i]);
                }
            }

            Mapa->BodyMin3Count = 0;
            Mapa->BodyMin4Count = 0;
            uint32_t pocitadloBodov= 0;
            /*
            * Spocitam pocet bodov s MIN 3 a 4 useckami
            */
            for (i = 0; i < Mapa->BodyCount; i++)
            {
                for (j = 0; j < Mapa->HranaCount; j++)
                {
                    if (Mapa->Body[i] == Mapa->Hrana[0][j] || Mapa->Body[i] == Mapa->Hrana[1][j])
                    {
                        pocitadloBodov++;
                    }
                }
                //pocet bodov s MIN 3 useckami
                if (pocitadloBodov >= 3 )
                {
                    Mapa->BodyMin3Count++;
                    //pocet bodov s MIN 4 useckami
                    if (pocitadloBodov >= 4 )
                    {
                        Mapa->BodyMin4Count++;
                    }
                }
                pocitadloBodov = 0;
            }

            if (DEBUG)
            {
                printf("\nBody s poctom useciek MIN 3 (BodyMin3Count) je: %d\n",Mapa->BodyMin3Count );
                printf("Body s poctom useciek MIN 4 (BodyMin4Count) je: %d\n",Mapa->BodyMin4Count );
            }

            Mapa->BodyMin3 = malloc(sizeof(uint32_t)*Mapa->BodyMin3Count);
            if (Mapa->BodyMin3 == NULL){
                perror("Nepodarilo sa alokovat miesto v pamati ... program skonci !\n");
                return false; // TODO error
            }
            Mapa->BodyMin4 = malloc(sizeof(uint32_t)*Mapa->BodyMin4Count);
            if (Mapa->BodyMin4 == NULL){
                perror("Nepodarilo sa alokovat miesto v pamati ... program skonci !\n");
                return false; // TODO error
            }

            pocitadloBodov = 0;
            uint32_t b3 = 0;
            uint32_t b4 = 0;

            /*
            * Ulozim do POLA body, ktore maju pocet useciek MIN 3 a 4
            */

            for (i = 0; i < Mapa->BodyCount; i++)
            {
                for (j = 0; j < Mapa->HranaCount; j++)
                {
                    if (Mapa->Body[i] == Mapa->Hrana[0][j] || Mapa->Body[i] == Mapa->Hrana[1][j])
                    {
                        pocitadloBodov++;
                    }
                }
                if (pocitadloBodov >= 3 )
                {
                    //Body s poctom useciek MIN 3
                    Mapa->BodyMin3[b3] = Mapa->Body[i];
                    b3++;
                    if (pocitadloBodov >= 4 )
                    {
                        //Body s poctom useciek MIN 4
                        Mapa->BodyMin4[b4] = Mapa->Body[i];
                        b4++;
                    }
                }
                pocitadloBodov = 0;
            }
            if (DEBUG)
            {
                printf("\nBody s poctom useciek MIN 3 (*BodyMin3) je :\n");
                // Body s poctom useciek MIN 3
                for (i = 0; i < Mapa->BodyMin3Count; i++)
                {
                    printf("\t\t\t\t%d\n", Mapa->BodyMin3[i]);
                }

                printf("\nBody s poctom useciek MIN 4 (*BodyMin4) je :\n");
                // Body s poctom useciek MIN 4
                for (i = 0; i < Mapa->BodyMin4Count; i++)
                {
                    printf("\t\t\t\t%d\n", Mapa->BodyMin4[i]);
                }
            }
        }
    }
    else /* fopen() returned NULL */{
        printf("nacitane suboru prebehlo NEKOREKTNE ... program skonci !\n");
        return false;
    }
    return true;
}

uint32_t * shellSort(uint32_t * array, uint32_t size) {
   uint32_t gap = size / 2;
   while (gap > 0) { //dokud mame co porovnavat
       for (uint32_t i = 0; i < size - gap; i++) { //upraveny insertion sort
           uint32_t j = i + gap;
           uint32_t tmp = array[j];
           while (j >= gap && tmp > array[j - gap]) {
               array[j] = array[j - gap];
               j -= gap;
           }
           array[j] = tmp;
       }
       if (gap == 2) { //zmena velikosti mezery
           gap = 1;
       } else {
           gap /= 2.2;
       }
   }
   return array;
}

void freeFunc(MapaStruct *Mapa){
    if (Mapa->Body != NULL)
    {
        free(Mapa->Body);
        Mapa->Body = NULL;
    }
    if (Mapa->BodyMin3 != NULL)
    {
        free(Mapa->BodyMin3);
        Mapa->BodyMin3 = NULL;
    }
    if (Mapa->BodyMin4 != NULL)
    {
        free(Mapa->BodyMin4);
        Mapa->BodyMin4 = NULL;
    }
    if (Mapa->Hrana[0] != NULL)
    {
        free(Mapa->Hrana[0]);
        Mapa->Hrana[0] = NULL;
    }
    if (Mapa->Hrana[1] != NULL)
    {
        free(Mapa->Hrana[1]);
        Mapa->Hrana[1] = NULL;
    }
}
