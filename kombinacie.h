/*********************************************************
* Subor: kombinacie.h                                    *
* Nazov projektu: IAL - Rovinnost grafu                  *
* Varianta: cislo 5                                      *
*                                                        *
* Vypracovali:                                           *
* 1. Kello Tomas,       xkello00@stud.fit.vutbr.cz  20%  *
* 2. Luptak Matej,      xlupta02@stud.fit.vutbr.cz  20%  *
* 3. Picha Vojtech,     xpicha06@stud.fit.vutbr.cz  20%  *
* 4. Skalnikova Zuzana, xskaln04@stud.fit.vutbr.cz  20%  *
* 5. Toth Viktor,       xtothv01@stud.fit.vutbr.cz  20%  *
*                                                        *
* Datum vytvorenia:         12.10.2015                   *
* Datum poslednej zmeny:    13.12.2015                   *
*********************************************************/
#ifndef KOMBINACIE_File
#define KOMBINACIE_File

#include "main.h"

bool NextComb (uint32_t *pole, uint32_t len, uint32_t max);

#endif
