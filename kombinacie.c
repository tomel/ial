/*********************************************************
* Subor: kombinacie.c                                    *
* Nazov projektu: IAL - Rovinnost grafu                  *
* Varianta: cislo 5                                      *
*                                                        *
* Vypracovali:                                           *
* 1. Kello Tomas,       xkello00@stud.fit.vutbr.cz  20%  *
* 2. Luptak Matej,      xlupta02@stud.fit.vutbr.cz  20%  *
* 3. Picha Vojtech,     xpicha06@stud.fit.vutbr.cz  20%  *
* 4. Skalnikova Zuzana, xskaln04@stud.fit.vutbr.cz  20%  *
* 5. Toth Viktor,       xtothv01@stud.fit.vutbr.cz  20%  *
*                                                        *
* Datum vytvorenia:         12.10.2015                   *
* Datum poslednej zmeny:    13.12.2015                   *
*********************************************************/
#include "K33.h"


bool NextComb3 (uint32_t *k33, uint32_t MAX, uint32_t start)
{
	if(start+3 >= MAX)
		return false; // do rozmedzia sa nezmesti ziadna kombinacia


	if(k33[2] == MAX-1)
	{ // treti index dosiahol konca, potrebne zmenit aj ten druhy
		if(k33[1] == MAX-2)
		{ // aj druhy index dosiahol konca, potrebne zmenit aj ten prvy
			if(k33[0] == MAX-3)
			{ // aj prvy index dosiahol konca, hodnoty sa resetuju na zaciatok
				// a vracia FALSE pre neplatnu moznost
				k33[0] = 0 +start;
				k33[1] = 1 +start;
				k33[2] = 2 +start;
				return false;
			}
			else
			{ // prvy index inkrementuje a ostatne zaradi zan
				k33[0] += 1;
				k33[1]	= k33[0]+1;
				k33[2]	= k33[1]+1;
			}
		}
		else
		{ // druhy index inkrementuje a treti zaradi zan
			k33[1] += 1;
			k33[2]	= k33[1] +1;
		}
	}
	else
		// inkrementuje len treti index
		k33[2] += 1;
	return true;
}

// len dlzka pola napr 3 pre pole 0,1,2
// max rozsah kobinacii. zacina sa od 0 po max
// napr 3 -> kobinacie (0,1,2) az (1,2,3)

int MoveNumber(uint32_t *pole, uint32_t len, uint32_t max)
{
	if (pole[len-1] == max)
	{
		pole[len-1] = MoveNumber(pole, len-1, max -1); // dalsia hodnota musia byt o 1 mensia ako aktualna
		pole[len-1]++; // hodnota musi byt o 1 vecsia ako predchadzajuca
	}
	else
		pole[len-1]++; // zvys hodnotu prvku
	return pole[len-1];
}

bool NextComb (uint32_t *pole, uint32_t len, uint32_t max)
{
	if(pole[0] == max-(len-1)) // prva hodnota dosiahla koniec
	{
		// reset values
		for (uint32_t i = 0; i < len; ++i)
			pole[i] = i;
		return false;
	}
	else
	{
		MoveNumber(pole, len, max);
		return true;
	}
}
