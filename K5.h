/*********************************************************
* Subor: K5.h                                            *
* Nazov projektu: IAL - Rovinnost grafu                  *
* Varianta: cislo 5                                      *
*                                                        *
* Vypracovali:                                           *
* 1. Kello Tomas,       xkello00@stud.fit.vutbr.cz  20%  *
* 2. Luptak Matej,      xlupta02@stud.fit.vutbr.cz  20%  *
* 3. Picha Vojtech,     xpicha06@stud.fit.vutbr.cz  20%  *
* 4. Skalnikova Zuzana, xskaln04@stud.fit.vutbr.cz  20%  *
* 5. Toth Viktor,       xtothv01@stud.fit.vutbr.cz  20%  *
*                                                        *
* Datum vytvorenia:         12.10.2015                   *
* Datum poslednej zmeny:    13.12.2015                   *
*********************************************************/
#ifndef K5File
#define K5File
#include "main.h"

// vyhlada v strukture Mapa spojenie medzi VSETKYMI bodmi k5[5]
// ak najde vypise vsetky body a vracia TRUE
// inak vracia FALSE
bool FindPath5(MapaStruct *Mapa, uint32_t *k5);

// najdi v strukture bodov a hran
// izomorfnu podmapu K5
int NajdiK5(MapaStruct *Mapa);

#endif
