/*********************************************************
* Subor: K33.c                                           *
* Nazov projektu: IAL - Rovinnost grafu                  *
* Varianta: cislo 5                                      *
*                                                        *
* Vypracovali:                                           *
* 1. Kello Tomas,       xkello00@stud.fit.vutbr.cz  20%  *
* 2. Luptak Matej,      xlupta02@stud.fit.vutbr.cz  20%  *
* 3. Picha Vojtech,     xpicha06@stud.fit.vutbr.cz  20%  *
* 4. Skalnikova Zuzana, xskaln04@stud.fit.vutbr.cz  20%  *
* 5. Toth Viktor,       xtothv01@stud.fit.vutbr.cz  20%  *
*                                                        *
* Datum vytvorenia:         12.10.2015                   *
* Datum poslednej zmeny:    13.12.2015                   *
*********************************************************/
#include "K33.h"



//hlada vsetky spojenia bodov pre k33
bool FindPath3(MapaStruct *Mapa, uint32_t k3a1, uint32_t k3a2, uint32_t k3a3, uint32_t k3b1, uint32_t k3b2, uint32_t k3b3)
{
	uint32_t a = 0;
	uint32_t b = 0;
	uint32_t c = 0;
	uint32_t d = 0;
	uint32_t e = 0;
	uint32_t f = 0;
	uint32_t g = 0;
	uint32_t h = 0;
	uint32_t k, l, m, n, o, p, r, s, t, u;
	k=l=m=n=o=p=r=s=t=u=0;
  
	for (uint32_t j = 0; j < Mapa->HranaCount; j++)
	{
		if ((Mapa->BodyMin3[k3a1] == Mapa->Hrana[0][j]) && (Mapa->Hrana[1][j] == Mapa->BodyMin3[k3b1]))
			a = 1;
		if ((Mapa->BodyMin3[k3a1] == Mapa->Hrana[0][j]) && (Mapa->Hrana[1][j] == Mapa->BodyMin3[k3b2]))
			b = 1;
		if ((Mapa->BodyMin3[k3a1] == Mapa->Hrana[0][j]) && (Mapa->Hrana[1][j] == Mapa->BodyMin3[k3b3]))
			c = 1;
		if ((Mapa->BodyMin3[k3a2] == Mapa->Hrana[0][j]) && (Mapa->Hrana[1][j] == Mapa->BodyMin3[k3b1]))
			d = 1;
		if ((Mapa->BodyMin3[k3a2] == Mapa->Hrana[0][j]) && (Mapa->Hrana[1][j] == Mapa->BodyMin3[k3b2]))
			e = 1;
		if ((Mapa->BodyMin3[k3a2] == Mapa->Hrana[0][j]) && (Mapa->Hrana[1][j] == Mapa->BodyMin3[k3b3]))
			f = 1;
		if ((Mapa->BodyMin3[k3a3] == Mapa->Hrana[0][j]) && (Mapa->Hrana[1][j] == Mapa->BodyMin3[k3b1]))
			g = 1;
		if ((Mapa->BodyMin3[k3a3] == Mapa->Hrana[0][j]) && (Mapa->Hrana[1][j] == Mapa->BodyMin3[k3b2]))
			h = 1;
		if ((Mapa->BodyMin3[k3a3] == Mapa->Hrana[0][j]) && (Mapa->Hrana[1][j] == Mapa->BodyMin3[k3b3]))
			k = 1;
			
		if ((Mapa->BodyMin3[k3a1] == Mapa->Hrana[1][j]) && (Mapa->Hrana[0][j] == Mapa->BodyMin3[k3b1]))
			l = 1;
		if ((Mapa->BodyMin3[k3a1] == Mapa->Hrana[1][j]) && (Mapa->Hrana[0][j] == Mapa->BodyMin3[k3b2]))
			m = 1;
		if ((Mapa->BodyMin3[k3a1] == Mapa->Hrana[1][j]) && (Mapa->Hrana[0][j] == Mapa->BodyMin3[k3b3]))
			n = 1;
		if ((Mapa->BodyMin3[k3a2] == Mapa->Hrana[1][j]) && (Mapa->Hrana[0][j] == Mapa->BodyMin3[k3b1]))
			o = 1;
		if ((Mapa->BodyMin3[k3a2] == Mapa->Hrana[1][j]) && (Mapa->Hrana[0][j] == Mapa->BodyMin3[k3b2]))
			p = 1;
		if ((Mapa->BodyMin3[k3a2] == Mapa->Hrana[1][j]) && (Mapa->Hrana[0][j] == Mapa->BodyMin3[k3b3]))
			r = 1;
		if ((Mapa->BodyMin3[k3a3] == Mapa->Hrana[1][j]) && (Mapa->Hrana[0][j] == Mapa->BodyMin3[k3b1]))
			s = 1;
		if ((Mapa->BodyMin3[k3a3] == Mapa->Hrana[1][j]) && (Mapa->Hrana[0][j] == Mapa->BodyMin3[k3b2]))
			t = 1;
		if ((Mapa->BodyMin3[k3a3] == Mapa->Hrana[1][j]) && (Mapa->Hrana[0][j] == Mapa->BodyMin3[k3b3]))
			u = 1;
	}
	
	if((( a & b & c & d & e & f & g & h & k) == 1 ) || (( l & m & n & o & p & r& s& t & u) ==1))

		return true;
	else
		return false;
}

// nastavi k3b na indexy v rozsahu 0-5 ktore sa nevyskytuju v k3a
void set_k3b(uint32_t *k3a, uint32_t *k3b)
{
	uint32_t indA = 0, indB = 0;
	for (uint32_t i = 0; i < 6; ++i)
	{
		if (i == k3a[indA])
			indA++;
		else
			k3b[indB++] = i;
	}
}

// vytvori dve skupiny (velkost skupiny 3) indexov a hlada ci body vytvaraju graf K33
uint32_t NajdiK33(MapaStruct *Mapa)
{
	if (DEBUG_K33)
		printf("----  K33 ----\nBody min 3 pocet: %u (musi byt aspon 6)\n", Mapa->BodyMin3Count);
	if (Mapa->BodyMin3Count < 6)
		return 0; // najdenych 0 kombinacii

	// definovanie prvej kombinacie
	uint32_t k6[] = { 0, 1, 2, 3, 4, 5 };
	uint32_t k3a[] = { 0, 1, 2 }; // tri indexy 0-5
	uint32_t k3b[] = { 3, 4, 5 }; // zvysne tri indexy ktore nie su v k3a


	uint32_t pocetKombinacii = 0;


	uint32_t K33Count = 0; // pocet najdenych izomorvnych podgrafou s K33

	while (1)// vytvara vsetky kombinacie sestic
	{

		while (1) // vytvaranie trojic v sestici
		{// vytvara vsetky kombinacie v k33b s hodnotami o 3 menej(indexy k33a)
			pocetKombinacii++;
			if (DEBUG_K33)
				printf("%u kombinacia: %u %u %u -- %u %u %u (hodnoty su INDEXY do mapa->BodyMin3)\n", pocetKombinacii,
                    k6[k3a[0]],
                    k6[k3a[1]],
                    k6[k3a[2]],
                    k6[k3b[0]],
                    k6[k3b[1]],
                    k6[k3b[2]]);
			if(FindPath3(Mapa,
				k6[k3a[0]],
				k6[k3a[1]],
				k6[k3a[2]],
				k6[k3b[0]],
				k6[k3b[1]],
				k6[k3b[2]])
				)
					K33Count++; // naslo izomorfny podgraf
			NextComb(k3a, 3, 5);
			if (k3a[0] != 0)
            {
                k3a[0] = 0;
                k3a[1] = 1;
                k3a[2] = 2;
                set_k3b(k3a, k3b);
				break;
            }
			set_k3b(k3a, k3b);
		} // koniec vytvarania trojic v sesticiach

		if (!NextComb(k6, 6, Mapa->BodyMin3Count-1))
			break;
	} // koniec vytvarania sestic

	if(DEBUG)
		printf("pocet kombinacii: %u\n", pocetKombinacii);

	return K33Count;
}
