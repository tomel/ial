/*********************************************************
* Subor: struct.h                                        *
* Nazov projektu: IAL - Rovinnost grafu                  *
* Varianta: cislo 5                                      *
*                                                        *
* Vypracovali:                                           *
* 1. Kello Tomas,       xkello00@stud.fit.vutbr.cz  20%  *
* 2. Luptak Matej,      xlupta02@stud.fit.vutbr.cz  20%  *
* 3. Picha Vojtech,     xpicha06@stud.fit.vutbr.cz  20%  *
* 4. Skalnikova Zuzana, xskaln04@stud.fit.vutbr.cz  20%  *
* 5. Toth Viktor,       xtothv01@stud.fit.vutbr.cz  20%  *
*                                                        *
* Datum vytvorenia:         12.10.2015                   *
* Datum poslednej zmeny:    13.12.2015                   *
*********************************************************/
#ifndef structFile
#define structFile
#include <stdint.h>

/* ak chces si vypisat testovacie hodnoty daj ich do podmienky
	if(DEBUG)
		printf("Tu sa program dostal\n");
*/

// celkova struktura mapy
typedef struct
{
	uint32_t  BodyCount; // pocet bodov																OK
	uint32_t *Body; // definuje pouzite body v Mape (nesmu sa opakovat)								OK
	uint32_t  BodyMin3Count; // pocet bodov s MIN 3 useckami										OK
	uint32_t *BodyMin3; // Body s poctom useciek MIN 3												OK
	uint32_t  BodyMin4Count; // pocet bodov s MIN 4 useckami										OK
	uint32_t *BodyMin4; // Body s poctom useciek min 4												OK
	uint32_t  HranaCount; // pocet hran 															OK
	uint32_t *Hrana[2]; // definuje hrany medzi bodmi (vsetky body musia byt v poli Body)			OK
}MapaStruct;

#endif
